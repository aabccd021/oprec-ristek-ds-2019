
if __name__ == '__main__':

    import os

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from sklearn import linear_model
    from sklearn.base import TransformerMixin
    from sklearn.model_selection import cross_val_score
    from sklearn.pipeline import Pipeline
    from sklearn.preprocessing import PolynomialFeatures

    """
    Constant variables
    """
    DATA_DIR = './data'
    X_VAR = ['f1', 'f2', 'f3', 'f4', 'f5']
    Y_VAR = ['result']

    """
    Prepare data
    """
    # list project data files
    os.listdir(DATA_DIR)

    # read csv
    train_dataset = pd.read_csv(DATA_DIR + '/train.csv')
    train_dataset.head()
    train_dataset.shape

    test_dataset = pd.read_csv(DATA_DIR + '/test.csv')
    test_dataset.head()

    # check null columns
    null_cols = [col for col in train_dataset.columns if train_dataset[col].isnull().sum() > 0]
    null_cols

    train_df = train_dataset[X_VAR + Y_VAR]
    train_df.head()
    train_df.shape
    train_df.corr()


    def show_plot(feature):
        plt.scatter(train_df[feature], train_df['result'])
        plt.title('result Vs %s' % feature, fontsize=14)
        plt.xlabel(feature, fontsize=14)
        plt.ylabel("result", fontsize=14)
        plt.show()


    # show_plot("f1")
    # show_plot("f2")
    # show_plot("f3")
    # show_plot("f4")
    # show_plot("f5")

    """
    Data preprocess and feature selection
    """


    def show_diff(feature_name, title=""):
        plt.scatter(train_df[feature_name], train_df['result'], color='orange', label='outlier')
        plt.scatter(train_df_temp[feature_name], train_df_temp['result'])
        plt.title('result Vs %s %s' % (feature_name, title), fontsize=14)
        plt.xlabel(feature_name, fontsize=14)
        plt.ylabel("result", fontsize=14)
        plt.show()


    # Remove outliers by deviation standard of 3
    outliers_ds_threshold = 3
    train_df_temp = train_df.copy(deep=True)
    for column in train_df_temp.columns.values:
        train_df_temp = train_df_temp[
            np.abs(train_df_temp[column] - train_df_temp[column].mean()) <= (
                    outliers_ds_threshold * train_df_temp[column].std())]

    # show_diff('f1')
    # show_diff('f2')
    # show_diff('f3')
    # show_diff('f4')
    # show_diff('f5')

    # Removes outliers using local outlier factor
    # from sklearn.neighbors import LocalOutlierFactor
    #
    # clf = LocalOutlierFactor(contamination=0.01)
    # train_df_temp = train_df.copy(deep=True)
    # for column in train_df_temp.columns.values:
    #     train_df_temp = train_df_temp[
    #         pd.Series(np.abs(clf.fit_predict(train_df[[column, 'result']]) == 1))]
    #
    # method_name = "(Local Outlier Factor Elimination)"
    # show_diff('f1', method_name)
    # show_diff('f2', method_name)
    # show_diff('f3', method_name)
    # show_diff('f4', method_name)
    # show_diff('f5', method_name)

    # Manually removes outliers
    train_df_temp = train_df.copy(deep=True)
    train_df_temp = train_df_temp.loc[np.abs(train_df.f1 > -45)]
    train_df_temp = train_df_temp.loc[np.abs(train_df.f1 < 70)]
    train_df_temp = train_df_temp.loc[np.abs(train_df.f4 < 12)]

    method_name = "(Manual Outlier Elimination)"
    # show_diff('f1', method_name)
    # show_diff('f2', method_name)
    # show_diff('f3', method_name)
    # show_diff('f4', method_name)
    # show_diff('f5', method_name)

    train_df = train_df_temp

    """
    Model Decision
    """

    # find best fitting
    for poly in range(1, 4):
        model = Pipeline(
            steps=[('p', PolynomialFeatures(poly)), ('l', linear_model.LinearRegression())])
        scores = np.array([])
        for i in range(2, 21):
            scores = np.append(scores,
                               cross_val_score(model, train_df[X_VAR], train_df[Y_VAR], cv=i,
                                               scoring='neg_mean_squared_error'))
        print(str(poly) + " : " + str(-scores.mean()))


    class CustomTransformer(TransformerMixin):
        def transform(self, X, *_):
            a = X.copy(deep=True)
            a['f4'] = a['f4'] ** 2
            return a

        def fit(self, *_):
            return self


    models = {
        "Linear 2":
            Pipeline(steps=[('p', PolynomialFeatures(2)), ('l', linear_model.LinearRegression())]),
        "Ridge 2":
            Pipeline(steps=[('p', PolynomialFeatures(2)), ('l', linear_model.Ridge())]),
        "Lasso 2":
            Pipeline(steps=[('p', PolynomialFeatures(2)), ('l', linear_model.Lasso())]),
        "Linear Custom":
            Pipeline(steps=[('c', CustomTransformer()), ('l', linear_model.LinearRegression())]),
        "Ridge Custom":
            Pipeline(steps=[('c', CustomTransformer()), ('l', linear_model.Ridge())]),
        "Lasso Custom":
            Pipeline(steps=[('c', CustomTransformer()), ('l', linear_model.Lasso())]),
   }

    for name, model in models.items():
        scores = np.array([])
        for i in range(2, 21):
            scores = np.append(scores,
                               cross_val_score(model, train_df[X_VAR], train_df[Y_VAR], cv=i,
                                               scoring='neg_mean_squared_error'))
        print(name + " " + str(-scores.mean()))

    model = models["Linear 2"]
    model.fit(train_df[X_VAR], train_df[Y_VAR].values.ravel())
    yPrediction = model.predict(test_dataset[X_VAR]).ravel()
    yPrediction

    # create csv
    answer = pd.DataFrame({
        'id': test_dataset['id'],
        'result': yPrediction,
    })
    answer.head(5)
    answer.to_csv('./output/answer.csv', index=False)
