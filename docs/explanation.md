# Penjelasan Source Code

Dokumen ini akan menjelaskan code yang saya submit untuk prediksi pada [Ristek 
Data Science Open Recruitment 2019](https://www.kaggle.com/c/oprecristek19).

## Identitas

Nama  : Muhamad Abdurahman

NPM   : 1706040095

## Persiapan

### Mendeklarasikan variabel konstan yang dibutuhkan
```python
DATA_DIR = './data'
X_VAR = ['f1', 'f2', 'f3', 'f4', 'f5']
Y_VAR = ['result']
```

### Membaca data dari CSV
Membaca data dari file CSV yang diberikan, melihat awal data, dan ukuran data.
Lalu cek apakah ada kolom kosong.
```python
# list project data files
os.listdir(DATA_DIR)

# read csv
train_dataset = pd.read_csv(DATA_DIR + '/train.csv')
train_dataset.head()
train_dataset.shape

test_dataset = pd.read_csv(DATA_DIR + '/test.csv')
test_dataset.head()

# check null columns
null_cols = [col for col in train_dataset.columns if train_dataset[col].isnull().sum() > 0]
```

### Membuat Data Frame baru dan lihat korelasi dari setiap variabel
```python
train_df = train_dataset[X_VAR + Y_VAR]
train_df.head()
train_df.shape
train_df.corr()
```
Berikut adalah tabel korelasi antara variabel

|      |       f1|       f2|       f3|       f4|       f5|   result|
|------|---------|---------|---------|---------|---------|---------|
|f1    | 1.000000| 0.017515| 0.002358| 0.047872|-0.026916| 0.750969|
|f2    | 0.017515| 1.000000| 0.076062| 0.036957|-0.022963| 0.181935|
|f3    | 0.002358| 0.076062| 1.000000| 0.031126|-0.006543|-0.139795|
|f4    | 0.047872| 0.036957| 0.031126| 1.000000|-0.011352|-0.513429|
|f5    |-0.026916|-0.022963|-0.006543|-0.011352| 1.000000|-0.002689|
|result| 0.750969| 0.181935|-0.139795|-0.513429|-0.002689| 1.000000|

### Menampilkan plot
```python
def show_plot(feature):
    plt.scatter(train_df[feature], train_df['result'])
    plt.title('result Vs %s' % feature, fontsize=14)
    plt.xlabel(feature, fontsize=14)
    plt.ylabel("result", fontsize=14)
    plt.show()

show_plot("f1")
show_plot("f2")
show_plot("f3")
show_plot("f4")
show_plot("f5")
```

#### result vs f1

![image](ori-result-vs-f1.png)

Dari plot diatas, sekilas pandangan mata relasi f1 dan result terlihat linear. 
Dan sesuai dengan korelasinya yang besar, kaitan f1 dan result erat dilihat dari
data yang cenderung memusat.

#### result vs f2

![image](ori-result-vs-f2.png)

#### result vs f3

![image](ori-result-vs-f3.png)

Dari plot dapat dilihat data f3 adalah distrik, namun tidak jelas pola atau
derajat polinomial dari datanya.

#### result vs f4

![image](ori-result-vs-f4.png)

F4 terlihat memiliki derajat polinomial 2 (kuadratik). Dengan sample semakin
sedikit untuk data yang paling besar.

#### result vs f5

![image](ori-result-vs-f5.png)

## Preposes Data
Pada tahap ini akan dilakukan filtering data untuk menghilangkan noise atau
outlier untuk prediksi data yang lebih akurat.
Standardisasi data tidak dilakukan karena setelah mencoba, hasil training tidak 
berubah.
Feature selection tidak dilakukan karena dari tabel korelasi terlihat tidak ada
data yang duplikat ataupun saling dependen sesama fitur.

Plot hasil filtering sementara akan dilihat dengan fungsi berikut.
```python
def show_diff(feature_name, title=""):
    plt.scatter(train_df[feature_name], train_df['result'], color='orange', label='outlier')
    plt.scatter(train_df_temp[feature_name], train_df_temp['result'])
    plt.title('result Vs %s %s' % (feature_name, title), fontsize=14)
    plt.xlabel(feature_name, fontsize=14)
    plt.ylabel("result", fontsize=14)
    plt.show()
```

### Percobaan 1: Menghilangkan outlier dengan standar deviasi yang besar
Pada code dibawah ini, data dengan standar deviasi untuk masing-masing variabel 
akan dihilangkan. Ternyata hal ini tidak efektif karena perlu penyesuaian secara
manual
```python
outliers_ds_threshold = 3
train_df_temp = train_df.copy(deep=True)
for column in train_df_temp.columns.values:
    train_df_temp = train_df_temp[
        np.abs(train_df_temp[column] - train_df_temp[column].mean()) <= (
                outliers_ds_threshold * train_df_temp[column].std())]
```

### Percobaan 2: Menghilangkan outliers dengan method bawaan Sklearn
```python
from sklearn.neighbors import LocalOutlierFactor

clf = LocalOutlierFactor(contamination=0.01)
train_df_temp = train_df.copy(deep=True)
for column in train_df_temp.columns.values:
    train_df_temp = train_df_temp[
        pd.Series(np.abs(clf.fit_predict(train_df[[column, 'result']]) == 1))]
```
Dengan metode ini, dengan faktor kontaminasi 0.1 didapatkan hasil yang sekilas 
baik dan bersih seperti yang terlihat pada **result vs f1** dan **result vs f3**
.

![image](lofe-result-vs-f1.png)
![image](lofe-result-vs-f3.png)


Tetapi, pada **result vs f4**, data yang penting pada bagian kanan bawah hilang.
![image](lofe-result-vs-f4.png)

Setelah mencoba beberapa prediksi, hal ini membuat prediksi menjadi tidak
akurat, bahkan tingkat akurasi menurun dibandingkan saat belum dihilankan
outliersnya.

### Percobaan 3: Menghilangkan outliers secara manual
Dengan manual mengeliminasi outliers tanpa menghilangkan sifat dari data 
tersebut.
```python
train_df_temp = train_df.copy(deep=True)
train_df_temp = train_df_temp.loc[np.abs(train_df.f1 > -45)]
train_df_temp = train_df_temp.loc[np.abs(train_df.f1 < 70)]
train_df_temp = train_df_temp.loc[np.abs(train_df.f4 < 12)]
```
berikut adalah data hasil eliminasi outliers secara manual.

![image](man-result-vs-f1.png)
![image](man-result-vs-f2.png)
![image](man-result-vs-f3.png)
![image](man-result-vs-f4.png)
![image](man-result-vs-f5.png)


## Penentuan Model
Prediksi ini dilakukan pada data yang kontinyu, sehingga model yang layak 
digunakan adalah model regresi. Model-model yang akan digunakan untuk diukur 
akurasinya adalah *linear regression*, *ridge regression*, *lasso regression*, 
dan satu model custom buatan sendiri.

### Mencari dejarat polynomial yang sesuai
Akan dicari derajat polynomial agar training yang dilakukan tidak overfit atau   
underfit. Derajat dengan *root mean squared error* terkecil hasil *cross
validation* dengan regresi linier akan digunakan.
```python
for poly in range(1, 4):
    model = Pipeline(
        steps=[('p', PolynomialFeatures(poly)), ('l', linear_model.LinearRegression())])
    scores = np.array([])
    for i in range(2, 21):
        scores = np.append(scores,
                           cross_val_score(model, train_df[X_VAR], train_df[Y_VAR], cv=i,
                                           scoring='neg_mean_squared_error'))
    print(str(poly) + " : " + str(-scores.mean()))
```
Berikut adalah output dari *code* di atas.

|Derajat|            *RMSE*|
|-------|------------------|
|      1|65.66579430472716 |
|      2|22.3448366256694  |
|      3|26.919241404567327|

Dari data diatas dapat dilihat derajat polynomial yang sesuai adalah 2.

### Membuat custom model sesuai hipotesis

```python
class CustomTransformer(TransformerMixin):
    def transform(self, X, *_):
        a = X.copy(deep=True)
        a['f4'] = a['f4'] ** 2
        return a

    def fit(self, *_):
        return self
```

Polynomial Features yang dilakukan sebelum regresi memungkinkan prediksi dapat 
dilakukan secara polynomial. Sedangkan pada model yang saya buat sendiri, *f4* 
dikuadratkan terlebih dahulu, dengan perkiraan *f4* fit dengan persamaan 
kuadrat.

### Memilih model terbaik dari sklear.linear_model

Lalu semua model diatas dilakukan peilaian dengan cross validation dengan
variasi pembagian kelompok 2-21. Penilaian dilakukan dengan mencari rata-rata
*root mean squared error* terkecil.
```python
models = {
        "Linear 2":
            Pipeline(steps=[('p', PolynomialFeatures(2)), ('l', linear_model.LinearRegression())]),
        "Ridge 2":
            Pipeline(steps=[('p', PolynomialFeatures(2)), ('l', linear_model.Ridge())]),
        "Lasso 2":
            Pipeline(steps=[('p', PolynomialFeatures(2)), ('l', linear_model.Lasso())]),
        "Linear Custom":
            Pipeline(steps=[('c', CustomTransformer()), ('l', linear_model.LinearRegression())]),
        "Ridge Custom":
            Pipeline(steps=[('c', CustomTransformer()), ('l', linear_model.Ridge())]),
        "Lasso Custom":
            Pipeline(steps=[('c', CustomTransformer()), ('l', linear_model.Lasso())]),
   }
   
for name, model in models.items():
    scores = np.array([])
    for i in range(2, 21):
        scores = np.append(scores,
                           cross_val_score(model, train_df[X_VAR], train_df[Y_VAR], cv=i,
                                           scoring='neg_mean_squared_error'))
    print(name + " " + str(-scores.mean()))
```
Dan berikut adalah hasil penilaian tersebut.

|        Model|            *RMSE*|
|-------------|------------------|
|     Linear 2|22.3448366256694  |
|      Ridge 2|22.335308976228923|
|      Lasso 2|22.744908185066162|
|Linear Custom|21.740086976549836|
|Ridge  Custom|21.740022220173785|
|Lasso  Custom|21.920655110273756|

Terlihat model custom dan *Ridge Regression* dengan *Polynomial Feature* 2 
memiliki akurasi yang tinggi.

## Menuliskan hasil prediksi
Memilih model berdasarkan hasil penilaian diatas, lalu melakukan prediksi data
test dengan model tersebut, lalu keluarkan sebagai file answer.csv
```python
model = models["Linear 2"]
model.fit(train_df[X_VAR], train_df[Y_VAR].values.ravel())
yPrediction = model.predict(test_dataset[X_VAR]).ravel()
yPrediction

# create csv
answer = pd.DataFrame({
    'id': test_dataset['id'],
    'result': yPrediction,
})
answer.head(5)
answer.to_csv('./output/answer.csv', index=False)
```
